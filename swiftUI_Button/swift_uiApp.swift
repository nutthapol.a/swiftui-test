//
//  swift_uiApp.swift
//  swift-ui
//
//  Created by Nutthapol on 7/10/2563 BE.
//

import SwiftUI

@main
struct swift_uiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
