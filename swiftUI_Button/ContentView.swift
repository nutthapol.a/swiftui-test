
import SwiftUI

struct ContentView: View {
  @State var isAlert = false
  @State var Clicked: Int = 0
  var body: some View {
    VStack {
      Text("Where is blue color ?")
        .padding()
        .font(.system(size: 30))
      
        .alert(isPresented: $isAlert) {
          Alert(title: Text("Your Answer"), message: Text("\(CheckAnswer(id: Clicked))"))
        }
    }
    HStack {
      Button(action: {
        self.isAlert = true
        self.Clicked = 1
      }){
        Text("RED")
          .padding()
          .background(Color.red)
          .foregroundColor(.white)
          .font(.system(size: 18))
      }
      Button(action: {
        self.isAlert = true
        self.Clicked = 2
      }){
        Text("BLUE")
          .padding()
          .background(Color.blue)
          .foregroundColor(.white)
          .font(.system(size: 18))
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}

func CheckAnswer(id: Int) -> (String)
{
  var msg:String
  if id == 2 {
    msg = "Correct!!!"
  } else {
    msg = "Nooob>>>>"
  }
  return(msg)
}
