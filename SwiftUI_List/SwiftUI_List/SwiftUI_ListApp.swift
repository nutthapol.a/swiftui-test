
import SwiftUI

@main
struct SwiftUI_ListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
