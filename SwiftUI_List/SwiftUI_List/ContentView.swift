
import SwiftUI

struct ContentView: View {
  let number = ["One","Two","Three","Four","Five"]
  let name = ["cat","dog","fish","cow"]
  var body: some View {
    
    VStack {
      NavigationView {
        VStack {
          List(number, id: \.self) {
            number in NavigationLink(
              destination: DetailView(chooseNumber: number)) {
              Text(number)
            }
          }
        }
        .navigationBarTitle("Number List")
      }
      NavigationView {
        VStack {
          List(name, id: \.self) {
            name in NavigationLink(
              destination: DetailView(chooseNumber: name)) {
              Text(name)
            }
          }
        }
        .navigationBarTitle("Name List")
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
