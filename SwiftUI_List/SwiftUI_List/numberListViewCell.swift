
import SwiftUI

struct DetailView: View {
  
  let chooseNumber: String
  
  @State var isAlert = false
  @State var clicked: Int = 0
  
  var body: some View {
    VStack {
      VStack {
        Text(chooseNumber)
          .padding()
          .font(.system(size: 30))
          
          .alert(isPresented: $isAlert) {
            Alert(title: Text("Your Answer"), message: Text("\(CheckAnswer(id: clicked))"))
          }
      }
      
      HStack {
        Button(action: {
          self.isAlert = true
          self.clicked = 1
        }){
          Text("RED")
            .padding()
            .background(Color.red)
            .foregroundColor(Color.white)
            .font(.system(size: 15))
        }
        
        Button(action: {
          self.isAlert = true
          self.clicked = 2
        }){
          Text("BLUE")
            .padding()
            .background(Color.blue)
            .foregroundColor(Color.white)
            .font(.system(size: 15))
        }
      }
    }
  }
}

func CheckAnswer(id: Int)-> (String)
{
  var msg:String
  if(id == 2){
    msg = "Correct!!!"
    
  }else{
    msg = "Wrong, Looser!"
  }
  
  return (msg)
}
